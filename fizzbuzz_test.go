package main

import (
    "net/http"
    "net/http/httptest"
    "testing"
)

func TestWelcome(t *testing.T) {
    req, err := http.NewRequest("GET", "/", nil)
    if err != nil {
        t.Fatal(err)
    }

    rr := httptest.NewRecorder()
    handler := http.HandlerFunc(Index)

    // Our handlers satisfy http.Handler, so we can call their ServeHTTP method 
    // directly and pass in our Request and ResponseRecorder.
    handler.ServeHTTP(rr, req)

    // Check the status code is what we expect.
    if status := rr.Code; status != http.StatusOK {
        t.Errorf("handler returned wrong status code: got %v want %v",
            status, http.StatusOK)
    }

    // Check the response body is what we expect.
    expected := `Welcome!

Please go to the /fizzbuzz url to test the API

This URL expect 5 GET parameters :
string1
string2
int1    
int2
limit(optional, default is 100, can't be over 1000)

Here are some examples :
    /fizzbuzz?string1=Hire&string2=Me&int1=5&int2=7
    /fizzbuzz?string1=I&string2=Like&int1=11&int2=2&limit=20
    /fizzbuzz?string1=Go&string2=lang&int1=3&int2=9&limit=200

This URL will count from 0 to limit where :
all multiples of int1 are replaced by string1,
all multiples of int2 are replaced by string2,
all multiples of int1 and int2 are replaced by string1string2

This URL response is a JSON containing an Array of strings
`
    if rr.Body.String() != expected {
        t.Errorf("handler returned unexpected body: got %v want %v",
            rr.Body.String(), expected)
    }


}

// TODO add multiple test, covering the error cases
// As this is a POC, i won't bother going deeper
func TestFizzBuzz(t *testing.T) {
    req, err := http.NewRequest("GET", "/fizzbuzz?string1=Go&string2=lang&int1=3&int2=4&limit=150", nil)
    if err != nil {
        t.Fatal(err)
    }
    rr := httptest.NewRecorder()
    handler := http.HandlerFunc(FizzBuzz)
    handler.ServeHTTP(rr, req)

    // TO DO content type detection

    if status := rr.Code; status != http.StatusOK {
        t.Errorf("handler returned wrong status code: got %v want %v",
            status, http.StatusOK)
    }

    expected := `["1","2","Go","lang","5","Go","7","lang","Go","10","11","Golang","13","14","Go","lang","17","Go","19","lang","Go","22","23","Golang","25","26","Go","lang","29","Go","31","lang","Go","34","35","Golang","37","38","Go","lang","41","Go","43","lang","Go","46","47","Golang","49","50","Go","lang","53","Go","55","lang","Go","58","59","Golang","61","62","Go","lang","65","Go","67","lang","Go","70","71","Golang","73","74","Go","lang","77","Go","79","lang","Go","82","83","Golang","85","86","Go","lang","89","Go","91","lang","Go","94","95","Golang","97","98","Go","lang","101","Go","103","lang","Go","106","107","Golang","109","110","Go","lang","113","Go","115","lang","Go","118","119","Golang","121","122","Go","lang","125","Go","127","lang","Go","130","131","Golang","133","134","Go","lang","137","Go","139","lang","Go","142","143","Golang","145","146","Go","lang","149","Go"]`
    if rr.Body.String() != expected {
        t.Errorf("handler returned unexpected body: got %v want %v",
            rr.Body.String(), expected)
    }
}