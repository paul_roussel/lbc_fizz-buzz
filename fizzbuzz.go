package main

import (
    "github.com/julienschmidt/httprouter"
    "net/http"
    "log"
    "strconv"
    "encoding/json"
)

func main() {
    // expose our endpoints
    router := httprouter.New()
    router.GET("/", IndexEndpoint)
    router.GET("/fizzbuzz", FizzBuzzEndpoint)

    log.Fatal(http.ListenAndServe(":8080", router))
}

/*
** theses functions exist to allow the router and the testing module to have different signatures
** It would better if they could share the same signature
*/
func IndexEndpoint(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
    Index(w, r);
}
func FizzBuzzEndpoint(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
    FizzBuzz(w, r);
}

/* / of our API */
func Index(w http.ResponseWriter, r *http.Request) {
    printHtmlResponse(w, `Welcome!

Please go to the /fizzbuzz url to test the API

This URL expect 5 GET parameters :
string1
string2
int1    
int2
limit(optional, default is 100, can't be over 1000)

Here are some examples :
    /fizzbuzz?string1=Hire&string2=Me&int1=5&int2=7
    /fizzbuzz?string1=I&string2=Like&int1=11&int2=2&limit=20
    /fizzbuzz?string1=Go&string2=lang&int1=3&int2=9&limit=200

This URL will count from 0 to limit where :
all multiples of int1 are replaced by string1,
all multiples of int2 are replaced by string2,
all multiples of int1 and int2 are replaced by string1string2

This URL response is a JSON containing an Array of strings
`)
}

/* /fizzbuzz of our API */
func FizzBuzz(w http.ResponseWriter, r *http.Request) {
    var string1, string2 string
    var int1, int2, limit uint64
    var err error = nil

    currentQuery := r.URL.Query()
    // makes sur every param is here and filled
    if (len(currentQuery.Get("int1")) < 1 || len(currentQuery.Get("int2"))  < 1 || len(currentQuery.Get("string1")) < 1 || len(currentQuery.Get("string2")) < 1) {
        printError(w, "missing or empty parameters\n")
        return
    }

    // get our replacing words
    string1 = currentQuery.Get("string1")
    string2 = currentQuery.Get("string2")

    // get our 2 multiples, ensure they are unsigned int
    int1, err = strconv.ParseUint(currentQuery.Get("int1"), 10, 64)
	if err != nil {
        printError(w, "Invalid parameter : int1\n")
        return
    }
    int2, err = strconv.ParseUint(currentQuery.Get("int2"), 10, 64)
	if err != nil {
        printError(w, "Invalid parameter : int2\n")
        return
	}

    // check if we have a limit, and if it's valid
    if len(currentQuery.Get("limit")) == 0 {
        limit = 100
    } else {
        limit, err = strconv.ParseUint(currentQuery.Get("limit"), 10, 64)
        if err != nil {
            printError(w, "Invalid parameter : limit\n")
            return
        }
        if limit > 1000 || limit < 1 {
            printError(w, "Invalid range for : limit\n")
            return
        }
    }

    // get a tab with our number transformed
    var proccessedNumbers = replaceMultiples(string1, string2, int1, int2, limit)

    jsonResponse, err := json.Marshal(proccessedNumbers);

    // write in the response
    printJSONResponse(w, jsonResponse);
}

func replaceMultiples(string1, string2 string, int1, int2, limit uint64) []string {
    var result = make([]string, limit)
    var currentNumber int
	for i := 0; i < int(limit); i++ {
        currentNumber = i + 1
        if isMultipleOf(currentNumber, int(int1)) && isMultipleOf(currentNumber, int(int2)) {
            result[i] = string1 + string2
        } else if isMultipleOf(currentNumber, int(int1)) {
            result[i] = string1
        } else if isMultipleOf(currentNumber, int(int2)) {
            result[i] = string2
        } else {
            result[i] = strconv.Itoa(currentNumber)
        }
	}
    return result
}

func isMultipleOf(number, multiple int) bool {
    return number % multiple == 0
}

func printError(w http.ResponseWriter, errorMessage string) {
    type jsonError struct {
		Message   string
    }
    currentError := jsonError{
		Message: errorMessage,
    }
    jsonResponse, err := json.Marshal(currentError)
	if err != nil {
        w.Header().Set("Content-Type","text/html")
        w.WriteHeader(http.StatusBadRequest)
        w.Write([]byte(errorMessage))
        return
	}
    w.Header().Set("Content-Type","application/json")
    w.WriteHeader(http.StatusBadRequest)
	w.Write(jsonResponse)
}

func printJSONResponse(w http.ResponseWriter, jsonResponse []byte) {
    w.Header().Set("Content-Type","application/json")
	w.WriteHeader(http.StatusOK)
	//Write json response back to response 
	w.Write(jsonResponse)
}


func printHtmlResponse(w http.ResponseWriter, htmlResponse string) {
    w.Header().Set("Content-Type","text/html")
	w.WriteHeader(http.StatusOK)
	//Write json response back to response 
	w.Write([]byte(htmlResponse))
}